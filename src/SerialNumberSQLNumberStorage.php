<?php

namespace Drupal\serial_number_generator;

use Drupal\Core\Database\Connection as DatabaseConnection;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Exception;

/**
 * Serial storage service definition.
 *
 * @todo before going further in other impl to SerialStorageInterface must be reviewed.
 *
 * SQL implementation for SerialStorage.
 */
class SerialNumberSQLNumberStorage implements SerialNumberStorageInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entiyFiledManager;

  /**
   * Drupal\Core\Language\LanguageManager definition.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Universal Table name Suffix.
   *
   * @var string
   */
  public static $universalTableSuffix = '_universal_serial_field';

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager, DatabaseConnection $connection, EntityFieldManager $entityFieldManager, LanguageManager $languageManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->connection = $connection;
    $this->entiyFiledManager = $entityFieldManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageNameFromField(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    return $this->createStorageName(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $fieldDefinition->getName(),
      (int) $fieldDefinition->getSetting(('universal_serial_field'))
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageName($entityTypeId, $entityBundle, $fieldName, $universalType) {
    // To make sure we don't end up with table names longer than 64 characters,
    // which is a MySQL limit we hash a combination of fields.
    // @todo Think about improvement for this.
    if ($universalType === 1) {
      $tableName = $entityTypeId . self::$universalTableSuffix;
    }
    else {
      $tableName = $entityTypeId . '_' . $entityBundle . '_' . $fieldName;
    }
    return $this->connection->escapeTable($tableName);
  }

  /**
   * {@inheritdoc}
   */
  public function generateValueFromName($storageName, $delete = TRUE) {
    $transaction = $this->connection->startTransaction();

    try {
      // Insert a temporary record to get a new unique serial value.
      $serialNumber = uniqid('', TRUE);
      $sid = $this->connection->insert($storageName)
        ->fields(['serial_number' => $serialNumber])
        ->execute();

      // If there's a reason why it's come back undefined, reset it.
      $sid = isset($sid) ? $sid : 0;

      // Delete the temporary record.
      if ($delete && $sid && ($sid % 10) == 0) {
        $this->connection->delete($storageName)
          ->condition('sid', $sid, '<')
          ->execute();
      }

      // Return the new unique serial value.
      return $sid;
    }
    // @todo use dedicated Exception
    // https://www.drupal.org/node/608166
    catch (\Exception $e) {
      $transaction->rollback();
      // @todo implement drupal logger here instead of watchdog.
      watchdog_exception('serial number', $e);
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateValue(FieldDefinitionInterface $fieldDefinition,
                                FieldableEntityInterface $entity,
                                $delete = TRUE) {
    $storageName = $this->createStorageNameFromField($fieldDefinition, $entity);
    return $this->generateValueFromName($storageName, $delete);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchema() {
    $schema = [
      'fields' => [
        'sid' => [
          'type' => 'serial',
          'not null' => TRUE,
          'unsigned' => TRUE,
          'description' => 'The atomic serial field.',
        ],
        'serial_number' => [
          'type' => 'varchar',
          'length' => 55,
          'default' => '',
          'not null' => TRUE,
          // @todo review UUID instead
          'description' => 'Unique temporary allocation Id.',
        ],
      ],
      'primary key' => ['sid'],
      'unique keys' => [
        'serial_number' => ['serial_number'],
      ],
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllFields() {
    return $this->entiyFiledManager->getFieldMapByFieldType(SerialNumberStorageInterface::SERIAL_FIELD_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function createStorage(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    $storageName = $this->createStorageNameFromField($fieldDefinition, $entity);
    $this->createStorageFromName($storageName);
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageFromName($storageName) {
    $dbSchema = $this->connection->schema();
    if (!$dbSchema->tableExists($storageName)) {
      $dbSchema->createTable($storageName, $this->getSchema());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function dropStorage(FieldDefinitionInterface $fieldDefinition, FieldableEntityInterface $entity) {
    $this->dropStorageFromName($this->createStorageNameFromField($fieldDefinition, $entity));
  }

  /**
   * {@inheritdoc}
   */
  public function dropStorageFromName($storageName) {
    $dbSchema = $this->connection->schema();
    $dbSchema->dropTable($storageName);
  }

  /**
   * {@inheritdoc}
   */
  public function initOldEntries($entityTypeId, $entityBundle, $fieldName, $startValue, $dependentFields, $noOfDegits, $entityIds) {
    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    $updated = 0;
    $storageName = $this->createStorageName(
      $entityTypeId,
      $entityBundle,
      $fieldName,
      0);
    foreach ($entityIds as $entityId) {
      $entity = $storage->loadUnchanged($entityId);
      $serial = $this->generateValueFromName($storageName);
      $entity->{$fieldName}->value = (string) str_pad(($startValue - 1) + $serial, $noOfDegits, '0', STR_PAD_LEFT);
      if ($entity->save() === SAVED_UPDATED) {
        ++$updated;
      }
    }
    return $updated;
  }

  /**
   * Gets the serial for this entity type, bundle, field instance.
   *
   * @return int
   *   serial id
   */
  public function getSerial($entity, $settings, $fieldDefinition) {
    $serial = NULL;
    $newSerial = FALSE;

    // Does not apply if the node is not new or translated.
    if ($entity->isNew()) {
      $newSerial = TRUE;
    }
    else {
      // Handle entity translation: fetch the same id or another one
      // depending of what is the design.
      // This should probably be solved by the end user decision
      // while setting the field translation.
      // @todo isMultilingual is global, prefer local hasTranslation
      if ($this->languageManager->isMultilingual() && $entity instanceof TranslatableInterface) {
        $newSerial = $entity->isNewTranslation();
      }
    }
    if ($newSerial) {
      /** @var \Drupal\serial\SerialStorageInterface $serialStorage */
      $serial = $this->generateValue($fieldDefinition, $entity);

      // Get the starting value from the storage settings.
      $startValue = isset($settings['start_value']) ? $settings['start_value'] : 1;
      $serial = ($startValue - 1) + $serial;
    }
    return $serial;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexFromName($storageName, $index) {
    $transaction = $this->connection->startTransaction();
    try {
      $this->connection->delete($storageName)
        ->condition('sid', $index, '<')
        ->execute();
      $this->connection->query("ALTER TABLE $storageName AUTO_INCREMENT= $index;
    ")->execute();
      return TRUE;
    }
    catch (Exception $e) {
      $transaction->rollBack();
      return FALSE;
    }
  }

}
