<?php

namespace Drupal\serial_number_generator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\serial_number_generator\SerialNumberSQLNumberStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class SerialIndexRestForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'serial_index_reset.settings';

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Use Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Use Drupal\serial_number_generator\SerialNumberSQLNumberStorage definition.
   *
   * @var \Drupal\serial_number_generator\SerialNumberSQLNumberStorage
   */
  protected $serialStorage;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entityTypeManager, EntityFieldManager $entityFieldManager, SerialNumberSQLNumberStorage $serialStorage) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->serialStorage = $serialStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('serial_number_generator.sql_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'serial_number_reset_index_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // $contentTypesList = [];
    // Get content Type information.
    foreach ($contentTypes as $contentType) {
      // $contentTypesList[$contentType->id()] = $contentType->label();
      $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions('node', $contentType->id());
      foreach ($fieldDefinitions as $fieldDefinition) {
        if ($fieldDefinition->getType() == 'serial_generator') {
          // $fieldName = $fieldDefinition->get('field_name');
          if ((int) $fieldDefinition->getSetting('universal_serial_field') === 1) {
            $universalContentType[$fieldDefinition->get('bundle')] = $fieldDefinition->get('bundle');
            $key = $fieldDefinition->get('entity_type') . '-universal_serial_field';
            $serialFields[$key] = 'Universal Serial Number';
            $universalFieldNames[$fieldDefinition->get('bundle')] = $fieldDefinition->get('field_name');
          }
          else {
            $key = $fieldDefinition->get('entity_type') . '-' . $fieldDefinition->get('bundle') . '-' . $fieldDefinition->get('field_name');
            $serialFields[$key] = $fieldDefinition->label() . ' (' . $contentType->id() . ')';
          }
        }
      }
    }

    $form['serial_field'] = [
      '#type'  => 'select',
      '#title'  => $this->t('Serial Field'),
      '#description'  => $this->t('Choose the Serial Field'),
      '#options'  => $serialFields,
      '#default_value'  => $config->get('serial_field'),
      '#required'  => TRUE,
    ];

    $form['serial_index'] = [
      '#type' => 'number',
      '#title' => $this->t('Index'),
      '#min' => 1,
      '#default_value' => $config->get('serial_index'),
    ];
    $form['universal_content_types'] = [
      '#type' => 'value',
      '#default_value' => $universalContentType,
    ];

    $form['universal_field_names'] = [
      '#type' => 'value',
      '#default_value' => $universalFieldNames,
    ];

    $form['universal_field_selected'] = [
      '#type' => 'hidden',
      '#default_value' => NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fieldDetails = explode("-", $form_state->getValue('serial_field'));
    if (isset($fieldDetails[2]) && !empty($fieldDetails[2])) {
      $fields[] = $fieldDetails[1];
    }
    else {
      $fields = $form_state->getValue('universal_content_types');
    }
    $query = $this->entityTypeManager->getStorage($fieldDetails[0]);
    $query_result = $query->getQuery()
      ->condition('status', 1)
      ->condition('type', $fields, 'IN')
      ->sort('nid', 'DESC')
      ->range(0, 1)
      ->execute();
    $nodes = $this->entityTypeManager
      ->getStorage($fieldDetails[0])
      ->load(reset($query_result));
    if (!isset($fieldDetails[2]) || empty($fieldDetails[2])) {
      $fieldDetails[2] = $form_state->getValue('universal_field_names')[$nodes->bundle()];
      $form_state->set('universal_field_selected', $fieldDetails[2]);
    }
    if ($nodes->get($fieldDetails[2])->getValue()[0]['value'] >= $form_state->getValue('serial_index')) {
      $form_state->setErrorByName('serial_index', $this->t('The is less than already existing index'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('universal_field_selected'))) {
      $tableName = str_replace('-', '_', $form_state->getValue('universal_field_selected'));
    }
    else {
      $tableName = str_replace('-', '_', $form_state->getValue('serial_field'));
    }
    $index = $form_state->getValue('serial_index');
    $reindexStatus = $this->serialStorage->alterIndexFromName($tableName, $index);
    if ($reindexStatus) {
      $this->messenger()->addStatus($this->t("The Index is reset to :index", [':index' => $index]));
    }
    else {
      $this->messenger()->addError($this->t("Couldn't Update Index"));
    }
  }

}
