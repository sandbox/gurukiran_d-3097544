<?php

namespace Drupal\serial_number_generator\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "serial_number_formatter",
 *   label = @Translation("Serial Number"),
 *   field_types = {
 *     "serial_generator"
 *   }
 * )
 */
class SerialDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $dependentFieldName = $items->getFieldDefinition()->getSettings()['dependent_fields'];
    $seperator = '';
    if ((int) $dependentFieldName === 0) {
      $dep = '';
    }
    else {
      $dep = $items->getEntity()->get($dependentFieldName)->getValue()[0]['value'];
      $seperator = $items->getFieldDefinition()->getSettings()['separator'];
    }
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => $dep . '' . $seperator . '' . $item->value,
      ];
    }

    return $element;
  }

}
