<?php

namespace Drupal\serial_number_generator\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\serial_number_generator\SerialNumberSQLNumberStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget bar.
 *
 * @FieldWidget(
 *   id = "serial_number_widget",
 *   label = @Translation("Serial Number"),
 *   field_types = {
 *     "serial_generator",
 *   }
 * )
 */
class SerialNumberWidget extends WidgetBase {

  /**
   * Use Drupal\serial_number_generator\SerialNumberSQLNumberStorage definition.
   *
   * @var \Drupal\serial_number_generator\SerialNumberSQLNumberStorage
   */
  protected $serialStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SerialNumberSQLNumberStorage $serial_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->serialStorage = $serial_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings']);
    $container->get('serial_number_generator.sql_storage')
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = $items->getSettings();
    $fieldName = $items->getName();
    $value = '';
    $counter = 0;
    $entity = $items->getEntity();
    if ($entity->id()) {
      $counter = $entity->get($fieldName)->getValue()[0]['value'];
    }
    else {
      $formBaseId = $form_state->getBuildInfo()['base_form_id'];
      if (!empty($form_state->getUserInput()) && ($formBaseId == 'node_form' || $formBaseId == 'user_form')) {
        $counter = $this->serialStorage->getSerial($entity, $settings, $items->getFieldDefinition());
      }
    }
    if ($counter) {
      $value = (string) str_pad($counter, $settings['no_of_digits'], '0', STR_PAD_LEFT);
    }
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#disabled' => TRUE,
    ];
    return ['value' => $element];
  }

}
