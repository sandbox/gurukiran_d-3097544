<?php

namespace Drupal\serial_number_generator\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of Serial Generator.
 *
 * @FieldType(
 *   id = "serial_generator",
 *   label = @Translation("Serial Number"),
 *   description = @Translation("Generate Roll Numbers based on the Fields."),
 *   default_formatter = "serial_number_formatter",
 *   default_widget = "serial_number_widget",
 * )
 */
class SerialNumberItem extends FieldItemBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 100,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'start_value' => 1,
      'dependent_fields' => '',
      'separator' => '',
      'no_of_digits' => 1,
      'universal_serial_field' => 0,
      'init_existing_entities' => 0,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $fields = $this->getEntity()->getFieldDefinitions();
    foreach ($fields as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getType() != 'serial_generator') {
        $bundleFields[0] = 'None';
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }
    $element = [];
    $element['start_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Starting value'),
      '#default_value' => $this->getSetting('start_value'),
      '#min' => 1,
      '#required' => TRUE,
      '#disabled' => $has_data,
    ];

    $element['universal_serial_field'] = [
      '#type' => 'radios',
      '#title' => $this->t('Universal Serial Field'),
      '#description' => $this->t('Whether this field should be used as universal serial field'),
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
      '#default_value' => $this->getSetting('universal_serial_field'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#attributes' => [
        'id' => 'field_universal_serial_field',
      ],
    ];

    $element['init_existing_entities'] = [
      '#type' => 'radios',
      '#title' => $this->t('Start on existing entities'),
      '#description' => $this->t('When this field is created for a bundle that already have entities.'),
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
      '#default_value' => $this->getSetting('init_existing_entities'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#states' => [
        'visible' => [
          ':input[id="field_universal_serial_field"]' => ['value' => 0],
        ],
      ],
    ];

    $element['dependent_fields'] = [
      '#title' => $this->t('Based On'),
      '#type' => 'select',
      '#description' => $this->t('Based on this field the Serial Number is genearted.'),
      '#options' => $bundleFields,
      '#default_value' => $this->getSetting('dependent_fields'),
    ];

    $element['separator'] = [
      '#title' => $this->t('Seperator'),
      '#type' => 'textfield',
      '#description' => $this->t('Seperator between Dependent field amd Serial Number.'),
      '#default_value' => $this->getSetting('separator'),
    ];

    $element['no_of_digits'] = [
      '#title' => $this->t('No Of Digits'),
      '#description' => $this->t('No. of Digits ate the end(Example CSE001 here the 3 is number of digits)'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $this->getSetting('no_of_digits'),
    ];

    // Only run the initialization when the field has no data.
    if (!$has_data) {
      // @todo ideally, use submit handler and not validate
      //   $handlers = $form_state->getSubmitHandlers();
      //   $handlers[] = [$this, 'initializeEntitiesCallback'];
      //   $form_state->setSubmitHandlers($handlers);
      $form['#validate'][] = [$this, 'initializeEntitiesCallback'];
    }
    return $element;
  }

  /**
   * Initialize entities depending on the storage settings.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   */
  public function initializeEntitiesCallback(array &$form, FormStateInterface $form_state) {
    // Check if existing entities have to be initialized.
    $settings = $form_state->getValue('settings');
    $fieldConfig = $this->getFieldDefinition();
    $serialStorage = \Drupal::getContainer()->get('serial_number_generator.sql_storage');
    $storageName = $serialStorage->createStorageName(
    $fieldConfig->getTargetEntityTypeId(),
    $fieldConfig->getTargetBundle(),
    $fieldConfig->getFieldStorageDefinition()->getName(),
    (int) $settings['universal_serial_field']
    );
    $serialStorage->createStorageFromName($storageName);

    if ((int) $settings['universal_serial_field'] === 0 && (int) $settings['init_existing_entities'] === 1) {
      $startValue = (int) $settings['start_value'];
      $dependentFields = $settings['dependent_fields'];
      $noOfDegits = $settings['no_of_digits'];
      // Check then first if the bundle has entities.
      $entityTypeId = $fieldConfig->getTargetEntityTypeId();
      $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
      $bundleKey = $storage->getEntityType()->getKey('bundle');
      $bundle = $fieldConfig->getTargetBundle();
      $query = \Drupal::entityQuery($entityTypeId);
      if ($entityTypeId != 'user') {
        $query->condition($bundleKey, $bundle);
      }
      $ids = $query->execute();
      $ids = array_diff($ids, [0]);
      if (count($ids) > 0) {
        /** @var \Drupal\serial\SerialStorageInterface $serialStorage */
        // Set serial values for existing entities.
        $oldCount = $serialStorage->initOldEntries(
          $entityTypeId,
          $bundle,
          $fieldConfig->getFieldStorageDefinition()->getName(),
          $startValue,
          $dependentFields,
          $noOfDegits,
          $ids
        );
        if ($oldCount > 0) {
          \Drupal::messenger()->addMessage($this->t('Serial values have been automatically set for %count existing entities, starting from %start_value.', [
            '%count' => $oldCount,
            '%start_value' => $startValue,
          ]));
        }
      }
      else {
        \Drupal::messenger()->addWarning($this->t('No entities to initialize, the next entity to be created will start from %start_value.', [
          '%start_value' => $startValue,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    foreach ($this->properties as $name => $property) {
      $value = $property->getValue();
      if (isset($this->values) || isset($value)) {
        $this->values[$name] = $value;
      }
    }
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Serial Number'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}
